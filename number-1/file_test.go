package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	NameTest string
	Input    [][]int
	Expected int
}

func TestMaxPathSum(t *testing.T) {

	test := []TestCase{
		{
			NameTest: "case example",
			Input: [][]int{
				{59},
				{73, 41},
				{52, 40, 9},
				{26, 53, 6, 34},
			},
			Expected: 237,
		},
		{
			NameTest: "case 1",
			Input: [][]int{
				{1},
				{2, 3},
				{4, 5, 6},
				{7, 8, 9, 10},
			},
			Expected: 20,
		},
		{
			NameTest: "case 3",
			Input: [][]int{
				{10},
				{15, 20},
				{44, 60, 90},
				{100, 600, 200, 1000},
				{10, 20, 30, 40, 50},
			},
			Expected: 1170,
		},
		{
			NameTest: "case 4",
			Input: [][]int{
				{10},
				{15, 20},
				{44, 60, 90},
				{100, 1000, 200, 600},
				{10, 20, 30, 40, 50},
			},
			Expected: 1120,
		},
	}

	for _, tc := range test {
		t.Run(tc.NameTest, func(t *testing.T) {
			result := maxPathSum(tc.Input)
			assert.Equal(t, tc.Expected, result)
		})
	}

}
