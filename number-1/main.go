package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main() {

	data, err := ioutil.ReadFile("hard.json")
	if err != nil {
		fmt.Println("เกิดข้อผิดพลาดในการอ่านไฟล์:", err)
	}

	var triangle [][]int
	err = json.Unmarshal(data, &triangle)
	if err != nil {
		fmt.Println("เกิดข้อผิดพลาดในการแปลง JSON:", err)
	}

	maxSum := maxPathSum(triangle)
	fmt.Println("Maximum path sum:", maxSum)
}

func maxPathSum(triangle [][]int) int {
	rows := len(triangle)
	sums := make([][]int, rows)
	for i := range sums {
		sums[i] = make([]int, i+1)
	}
	sums[0][0] = triangle[0][0]
	for i := 1; i < rows; i++ {
		for j := 0; j <= i; j++ {
			if j == 0 {
				sums[i][j] = sums[i-1][j] + triangle[i][j]
			} else if j == i {
				sums[i][j] = sums[i-1][j-1] + triangle[i][j]
			} else {
				sums[i][j] = max(sums[i-1][j-1], sums[i-1][j]) + triangle[i][j]
			}
		}
	}
	maxSum := 0
	for _, sum := range sums[rows-1] {
		if sum > maxSum {
			maxSum = sum
		}
	}
	return maxSum
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
