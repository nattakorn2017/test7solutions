package config

import (
	"log"
	"strings"

	"github.com/spf13/viper"
)

func Init() {
	initViper()
	loadConfig()
}

func initViper() {
	viper.AddConfigPath("config")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("connot read config in viper %v", err)
	}
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
}

func loadConfig() {
	Config = &RoadConfig{
		Serve: Runserve{
			Port: viper.GetString("SERVE.PORT"),
		},
		URL: viper.GetString("URL"),
	}
}
