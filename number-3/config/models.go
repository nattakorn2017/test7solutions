package config

var Config *RoadConfig

type RoadConfig struct {
	Serve Runserve
	URL   string
}

type Runserve struct {
	Port string
}
