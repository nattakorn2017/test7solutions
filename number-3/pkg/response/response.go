package response

import (
	"github.com/labstack/echo/v4"
)

type httpResponseTemplate struct {
	Result result      `json:"result"`
	Data   interface{} `json:"data,omitempty"`
}

type result struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func MakeHTTPSuccessResponse(c echo.Context, data interface{}) *httpResponseTemplate {
	return &httpResponseTemplate{
		Result: result{
			Code:    "0000",
			Message: "Success",
		},
		Data: data,
	}
}

func MakeHTTPFailedResponse(code, message string) *httpResponseTemplate {
	return &httpResponseTemplate{
		Result: result{
			Code:    code,
			Message: message,
		},
	}
}

type PingServe struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
