package main

import (
	"poc/test7Solution/number-3/app/beef"
	"poc/test7Solution/number-3/app/route"
	"poc/test7Solution/number-3/config"
	"poc/test7Solution/number-3/external/httpCall"
	"runtime"

	"github.com/labstack/echo/v4"
	echoMiddleWare "github.com/labstack/echo/v4/middleware"
)

func init() {
	runtime.GOMAXPROCS(1)
	config.Init()
}

func main() {
	// initail system
	e := initFramework()
	handle := initHandle(config.Config.URL)
	// run serve
	app := route.NewRoute(e, handle)
	app.Route()

}

func initHandle(url string) *beef.HTTPHandler {
	return beef.NewHTTPHandler(beef.NewUsecase(httpCall.NewHttpCall(url)))
}

func initFramework() *echo.Echo {
	e := echo.New()
	e.Use(echoMiddleWare.CORSWithConfig(echoMiddleWare.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
	}))

	e.Use(echoMiddleWare.Logger())
	e.Use(echoMiddleWare.Recover())

	return e
}
