package route

import (
	"log"
	"net/http"
	"poc/test7Solution/number-3/app/beef"
	"poc/test7Solution/number-3/config"
	"poc/test7Solution/number-3/pkg/response"

	"github.com/labstack/echo/v4"
)

type initailize struct {
	e      *echo.Echo
	handle *beef.HTTPHandler
}

func NewRoute(
	e *echo.Echo,
	handle *beef.HTTPHandler,
) *initailize {
	return &initailize{
		e:      e,
		handle: handle,
	}
}

func (i *initailize) Route() {

	// welcome message
	i.e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, response.MakeHTTPSuccessResponse(c, response.PingServe{Status: "OK", Message: "wellcome"}))
	})

	i.e.GET("/beef/summary", i.handle.GetSummaryBeef)

	go func() {
		log.Printf("app listening port :%v", config.Config.Serve.Port)
		log.Fatal(i.e.Start(":" + config.Config.Serve.Port))
	}()
	gracefulShutdown(i.e)

}
