package beef

import (
	"github.com/labstack/echo/v4"
)

type IUsecase interface {
	Usecases(c echo.Context) (map[string]int, error)
}

type IHTTP interface {
	Call() (string, error)
}
