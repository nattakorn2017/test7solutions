package beef

import (
	"log"
	"net/http"
	"poc/test7Solution/number-3/pkg/response"

	"github.com/labstack/echo/v4"
)

type HTTPHandler struct {
	usecase IUsecase
}

func NewHTTPHandler(usecase IUsecase) *HTTPHandler {
	return &HTTPHandler{
		usecase: usecase,
	}
}

func (h HTTPHandler) GetSummaryBeef(c echo.Context) error {
	result, errUsecase := h.usecase.Usecases(c)
	if errUsecase != nil {
		log.Printf("error get beef : %v", errUsecase)
		return c.JSON(http.StatusOK, response.MakeHTTPFailedResponse("FAIL", "9999"))
	}
	var resp BeefResp
	resp.Beef = result

	return c.JSON(http.StatusOK, response.MakeHTTPSuccessResponse(c, resp))
}
