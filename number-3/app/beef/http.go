package beef

type BeefResp struct {
	Beef map[string]int `json:"beef"`
}

type Beef struct {
	TBone    int `json:"t-bone"`
	Fatback  int `json:"fatback"`
	Pastrami int `json:"pastrami"`
	Pork     int `json:"pork"`
	Meatloaf int `json:"meatloaf"`
	Jowl     int `json:"jowl"`
	Enim     int `json:"enim"`
	Bresaola int `json:"bresaola"`
}
