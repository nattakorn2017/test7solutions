package beef_test

import (
	"net/http"
	"net/http/httptest"
	"poc/test7Solution/number-3/app/beef"
	"poc/test7Solution/number-3/app/beef/mock"

	"testing"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestUsecase(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mHTTP := mock.NewMockIHTTP(ctrl)
	usecase := beef.NewUsecase(mHTTP)

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	mHTTP.EXPECT().Call().Return("t-bone t-bone. t-bone., t-bone, bresaola bresaola pork bresaola bresaola pork", nil)

	expected := beef.BeefResp{
		Beef: map[string]int{
			"t-bone":   4,
			"bresaola": 4,
			"pork":     2,
		},
	}
	result, _ := usecase.Usecases(c)
	var resp beef.BeefResp
	resp.Beef = result

	assert.Equal(t, expected, resp)

}
