package beef

import (
	"strings"

	"github.com/labstack/echo/v4"
)

type Usecase struct {
	IhttpCall IHTTP
}

func NewUsecase(IhttpCall IHTTP) *Usecase {
	return &Usecase{IhttpCall: IhttpCall}
}

func (u *Usecase) Usecases(c echo.Context) (map[string]int, error) {
	groupText := make(map[string]int)
	data, err := u.IhttpCall.Call()
	if err != nil {
		return groupText, err
	}

	lowercaseMessage := strings.ToLower(data)
	text := strings.Fields(lowercaseMessage)

	for idx, t := range text {
		t = strings.Replace(t, ".", "", -1)
		t = strings.Replace(t, ",", "", -1)
		text[idx] = t
		groupText[t] = 0
	}

	for key, rs := range groupText {
		if rs != 0 {
			continue
		}
		count := SearchKey(text, key)
		groupText[key] = count
	}

	return groupText, nil
}

func SearchKey(text []string, keyword string) int {
	count := 0
	for _, word := range text {
		if word == keyword {
			count++
		}
	}
	return count

}
