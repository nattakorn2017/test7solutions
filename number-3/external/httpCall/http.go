package httpCall

import (
	"io/ioutil"
	"net/http"
)

type HTTP struct {
	url string
}

func NewHttpCall(url string) *HTTP {
	return &HTTP{url: url}
}

func (c *HTTP) Call() (string, error) {
	url := c.url
	method := "GET"
	var resp string
	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return resp, err
	}
	res, err := client.Do(req)
	if err != nil {
		return resp, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return resp, err
	}
	return string(body), err
}
