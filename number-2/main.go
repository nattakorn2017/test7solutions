package main

import (
	"fmt"
	"strings"
)

func main() {
	var input string
	fmt.Print("ป้อนข้อความที่เข้ารหัส: ")
	fmt.Scan(&input)
	encoded := encodeString(input)
	fmt.Println("ตัวเลขชุดที่ถูกแปลง:", encoded)
	decoded := decodeMinimum(encoded)
	fmt.Println("ตัวเลขชุดที่ถูกแปลง minimum:", decoded)

}

func encodeString(input string) string {
	output := ""
	for i := 0; i < len(input)-1; i++ {
		left := int(input[i])
		right := int(input[i+1])
		if left > right {
			output += "L"
		} else if right > left {
			output += "R"
		} else {
			output += "="
		}
	}
	return output
}

func decodeMinimum(str string) string {
	numbers := make([]int, len(str)+1)
	for i := 0; i < len(str); i++ {
		for j := 0; j < len(str); j++ {
			switch str[j : j+1] {
			case "L":
				for numbers[j] <= numbers[j+1] {
					numbers[j] = numbers[j] + 1
				}
			case "R":
				for numbers[j+1] <= numbers[j] {
					numbers[j+1] = numbers[j+1] + 1
				}
			case "=":
				if str[0] == '=' {
					numbers[j] = numbers[j+1]
				} else if str[len(str)-1] == '=' {
					numbers[j+1] = numbers[j]
				} else {
					numbers[j] = numbers[j+1]
				}
			}
		}
	}
	return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(numbers)), ""), "[]")

}
