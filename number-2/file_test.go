package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	NameTest string
	Input    string
	Expected string
}

func TestDecodeMinimum(t *testing.T) {

	test := []TestCase{
		{
			NameTest: "case 1",
			Input:    "LLRR=",
			Expected: "210122",
		},
		{
			NameTest: "case 2",
			Input:    "==RLL",
			Expected: "000210",
		},
		{
			NameTest: "case 3",
			Input:    "=LLRR",
			Expected: "221012",
		},
	}

	for _, tc := range test {
		t.Run(tc.NameTest, func(t *testing.T) {
			result := decodeMinimum(tc.Input)
			assert.Equal(t, tc.Expected, result)
		})
	}

}
